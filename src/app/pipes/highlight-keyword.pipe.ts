import { Pipe, PipeTransform } from '@angular/core';
import {Observable, of} from 'rxjs';

@Pipe({
  name: 'highlightKeyword'
})
export class HighlightKeywordPipe implements PipeTransform {

  transform(itemValue: string, keyword: string): Observable<string> {
    keyword = keyword.trim().replace(/([\[\].\-+{}()?\/:])/gim, '\\$1');
    const filterRegExp = new RegExp(keyword, 'ig');
    const filtered: string = itemValue.replace( filterRegExp, (match ) => {
      return match ? `<span class="emphasized">${match}</span>` : match;
    });
    return of(filtered);
  }

}
