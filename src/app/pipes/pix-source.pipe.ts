import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pixSource'
})
export class PixSourcePipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    if (!value) {
      return '../../../../../assets/images/default-pix.png';
    }
    return value;
  }

}
