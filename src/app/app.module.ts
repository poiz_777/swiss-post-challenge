import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { SearchFieldComponent } from './components/search-field/search-field.component';
import { SearchResultsComponent } from './components/search-results/search-results.component';
import {ErrorManager} from './services/error.manager.service';
import {ErrorMessageService} from './services/error.message.service';
import {SearchBrokerService} from './services/search-broker.service';
import {FormsModule} from '@angular/forms';
import { ItemDetailComponent } from './components/item-detail/item-detail.component';
import { PixSourcePipe } from './pipes/pix-source.pipe';
import { HighlightKeywordPipe } from './pipes/highlight-keyword.pipe';
import { HeaderComponent } from './components/header/header.component';
import { LogoComponent } from './components/units/logo/logo.component';
import { IconSearchComponent } from './components/units/icon-search/icon-search.component';
import { ArbitraryMenuComponent } from './components/arbitrary-menu/arbitrary-menu.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SearchFieldComponent,
    SearchResultsComponent,
    ItemDetailComponent,
    PixSourcePipe,
    HighlightKeywordPipe,
    HeaderComponent,
    LogoComponent,
    IconSearchComponent,
    ArbitraryMenuComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    ErrorManager,
    ErrorMessageService,
    SearchBrokerService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
