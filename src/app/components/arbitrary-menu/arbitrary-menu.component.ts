import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-arbitrary-menu',
  templateUrl: './arbitrary-menu.component.html',
  styleUrls: ['./arbitrary-menu.component.scss']
})
export class ArbitraryMenuComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  preventDefaultAnchorBehavior(event: Event): void {
    event.preventDefault();
  }

}
