import { AfterViewInit, Component, ElementRef, Input, ViewChild } from '@angular/core';
import {BeerInterface} from '../../interfaces/beer.interface';
import {SearchBrokerService} from '../../services/search-broker.service';
import {SearchResultsComponent} from '../search-results/search-results.component';
import {ValuesCentralService} from '../../services/values-central.service';
import {fromEvent, Subject} from 'rxjs';
import { debounceTime } from 'rxjs/operators';

interface EventKeywordInterface {
  event: KeyboardEvent | Event;
  keyword: string;
}

@Component({
  selector: 'app-search-field',
  templateUrl: './search-field.component.html',
  styleUrls: ['./search-field.component.scss']
})
export class SearchFieldComponent implements AfterViewInit {
  @ViewChild('keyword') keyword?: ElementRef;
  @ViewChild('searchResultsComponent') searchResultsComponent?: ElementRef;
  @Input() shouldRenderResult = true;
  public suggestedBeers: BeerInterface[] = [];
  public shouldFetchData = true;
  private subject: Subject<EventKeywordInterface> = new Subject<EventKeywordInterface>();

  constructor(private searchBrokerService: SearchBrokerService, private vcService: ValuesCentralService) { }

  ngAfterViewInit(): void {

    this.subject.pipe(
      debounceTime(500)
    ).subscribe(eKeyword => {
      this.fetchSuggestedBeers(eKeyword.event as KeyboardEvent, eKeyword.keyword);
    });
    this.vcService.abortOperationsSignal.subscribe(() => {
      if (this.keyword){
        this.keyword.nativeElement.value = '';
        this.resetSearchField( this.keyword.nativeElement, false);
      }
    });

    this.vcService.refocusSearchFieldSignal.subscribe(() => {
        if (this.keyword) {
          this.keyword.nativeElement.focus();
        }
    });

    this.vcService.itemWasSelectedSignal.subscribe((beer) => {
      this.updatePartialStateData(beer);
    });

    this.vcService.updateFieldValueSignal.subscribe((itemValue) => {
      this.updateSearchField(itemValue);
    });
  }

  onKeyUp(event: KeyboardEvent, keyword: string): void {
    this.subject.next({event, keyword});
  }
  fetchSuggestedBeers(event: KeyboardEvent, keyword: string): void {

    switch (event.key) {
      case 'Tab':
      case 'Down': // IE/EDGE SPECIFIC VALUE
      case 'ArrowDown':
        this.navigateToFirstResultRow();
        break;

      case 'Esc': // IE/EDGE SPECIFIC VALUE
      case 'Escape':
        this.resetSearchField(this.keyword?.nativeElement);
        break;

      case 'Up': // IE/EDGE SPECIFIC VALUE
      case 'Left': // IE/EDGE SPECIFIC VALUE
      case 'Right': // IE/EDGE SPECIFIC VALUE
      case 'ArrowUp':
      case 'ArrowLeft':
      case 'ArrowRight':
        // JUST DO NOTHING... ALL GOOD...  ;-)
        break;

      default:
        this.fetchResults(keyword);
    }
  }

  fetchResults(keyword: string): void {
    this.shouldFetchData = true;
    this.shouldRenderResult = true;
    keyword = keyword.trim();
    // CLEAR SUGGESTED BEERS IF LENGTH OF TRIMMED `keyword` IS LESS THAN 3
    this.suggestedBeers = [];

    // ONLY FETCH SUGGESTED BEERS IF LENGTH OF TRIMMED `keyword` IS GREATER THAN OR EQUAL TO 3.
    // MAKING FREQUENT API CALLS IS EXPENSIVE. IDEALLY, A STORE-SOLUTION LIKE «NGRX» WOULD SHINE OUT HERE...
    // IN COMBINATION WITH LOCAL OR SESSION STORAGE... UNFORTUNATELY, NO TIME TO IMPLEMENT THOSE [ TIME CONSTRAINT ].
    if ( keyword.length >= 3 ){
      this.searchBrokerService.fetchSuggestionsByKeyWord(keyword)
        .subscribe(beers => {
          this.suggestedBeers = beers;
        });
    }
  }

  navigateToFirstResultRow(): void {
    (this.searchResultsComponent as unknown as SearchResultsComponent).activateFirstResultsRow();
  }

  updatePartialStateData(beer: BeerInterface, broadcast = true): void{
    this.shouldFetchData = false;
    this.shouldRenderResult = false;
    if (broadcast){
      this.vcService.broadcastRenderDetailSignal(beer);
    }
    if (this.keyword){
      this.keyword.nativeElement.value = beer.name;
    }
  }

  updateSearchField(strValue: string): void{
    if (this.keyword){
      this.keyword.nativeElement.value = strValue;
    }
  }

  resetSearchField(keywordField: HTMLInputElement, broadcast = true): void{
    keywordField.value = '';
    this.shouldFetchData = false;
    this.shouldRenderResult = false;
    this.suggestedBeers = [];
    if ( broadcast) {
      this.vcService.broadcastRenderDetailSignal(undefined );
    }
  }

  repositionLabel(labelTextNode: HTMLElement , actionType: string): void{
    switch (actionType.toLowerCase()){
      case 'blur':
        if (labelTextNode.classList.contains('search-label__label-text--entered')) {
          if (!this.keyword?.nativeElement.value){
            labelTextNode.classList.remove('search-label__label-text--entered');
          }
        }
        break;
      case 'focus':
        if (!labelTextNode.classList.contains('search-label__label-text--entered')) {
          labelTextNode.classList.add('search-label__label-text--entered');
        }
        break;
    }
  }
}
