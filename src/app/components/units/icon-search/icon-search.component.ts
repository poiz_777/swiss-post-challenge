import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ValuesCentralService} from '../../../services/values-central.service';

@Component({
  selector: 'app-icon-search',
  templateUrl: './icon-search.component.html',
  styleUrls: ['./icon-search.component.scss']
})
export class IconSearchComponent implements OnInit {

  constructor(private vcService: ValuesCentralService) { }

  ngOnInit(): void {
  }

  dispatchSearchWasTriggeredSignal(event: Event): void {
    this.vcService.updateSearchButtonTriggeredStream(event);
  }

}
