import {Component, Input, ViewChild, ElementRef, AfterViewInit} from '@angular/core';
import {BeerInterface} from '../../interfaces/beer.interface';
import {ValuesCentralService} from '../../services/values-central.service';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss']
})
export class SearchResultsComponent implements AfterViewInit {
  @ViewChild('suggestedItemsCollection') suggestedItemsCollection?: ElementRef;
  @Input() suggestedBeers: BeerInterface[] = [];
  @Input() activeKeyword = '';
  @Input() resultsShouldBeRendered = true;
  currentTabIndex = 0;

  constructor(private vcService: ValuesCentralService) { }

  ngAfterViewInit(): void {
    /**
     * HIDE DROP-DOWN LIST OF SUGGESTED TERMS
     * WHEN YOU CLICK OUTSIDE THE ENTIRE WIDGET...
     * THIS IS A WORKAROUND FOR ON-CLICK-OUTSIDE EVENT
     * THAT I CONJURED UP ON THE FLY...
     * FEEL FREE TO IMPLEMENT A BETTER ONE IF YOU KNOW ANY ;-)
     */
    document.addEventListener('click', (e: MouseEvent) => {
      const resultsWrapper = this.suggestedItemsCollection?.nativeElement;
      if (resultsWrapper && !resultsWrapper.contains(e.target)){
        this.resultsShouldBeRendered = false;
      }
    });
  }

  handleKeyPressOnItemRow(event: KeyboardEvent, beer: BeerInterface): void{
    switch (event.key) {
      case 'Up': // IE/EDGE SPECIFIC VALUE
      case 'ArrowUp':
        this.navigateToPreviousResultRow(event);
        break;

      case 'Down': // IE/EDGE SPECIFIC VALUE
      case 'ArrowDown':
        this.navigateToNextResultRow(event);
        break;

      case 'Tab':
      case 'Enter':
        this.activateCurrentItemUnderFocus(event, beer);
        break;

      case 'Esc': // IE/EDGE SPECIFIC VALUE
      case 'Escape':
        this.vcService.broadcastAbortOperationsSignal(true);
        this.resultsShouldBeRendered = false;
        break;
    }
  }

  dispatchItemSelectedSignal(beer: BeerInterface): void{
    this.resultsShouldBeRendered = true;
    this.vcService.broadcastItemWasSelectedSignal(beer);
  }

  dispatchSearchFieldShouldUpdateSignal(beerTitle: string): void{
    this.vcService.broadcastUpdateFieldValueSignal(beerTitle);
  }

  private activateCurrentItemUnderFocus(event: KeyboardEvent, beer: BeerInterface): void {
    event.preventDefault();
    this.currentTabIndex = (event.target as HTMLElement).tabIndex;
    this.dispatchItemSelectedSignal(beer);
  }

  private navigateToNextResultRow(event: KeyboardEvent): void {
    const element: HTMLElement = event.target as HTMLElement;
    let nextSibling: HTMLElement | null | undefined = element.nextSibling as HTMLElement;
    nextSibling =  nextSibling.tabIndex ? nextSibling : this.getFirstRow();
    if ( nextSibling ) {
      nextSibling.focus();
      this.currentTabIndex = nextSibling.tabIndex;
      this.dispatchSearchFieldShouldUpdateSignal(nextSibling.getAttribute('data-title') as string);
    }
  }

  private navigateToPreviousResultRow(event: KeyboardEvent): void {
    const element: HTMLElement = event.target as HTMLElement;
    let previousSibling: HTMLElement | null | undefined = element.previousSibling as HTMLElement;
    previousSibling =  previousSibling && previousSibling.tabIndex !== undefined ? previousSibling : this.getLastRow();
    if ( previousSibling ) {
      previousSibling.focus();
      this.currentTabIndex = previousSibling.tabIndex;
      this.dispatchSearchFieldShouldUpdateSignal(previousSibling.getAttribute('data-title') as string);
    }
  }

  getFirstRow(): HTMLElement{
    const firstRow: HTMLElement = this.suggestedItemsCollection?.nativeElement.firstChild;
    this.currentTabIndex = firstRow.tabIndex;
    return firstRow;
  }

  getLastRow(): HTMLElement {
    const elementsCollection: Array<HTMLElement> = this.suggestedItemsCollection?.nativeElement.children;
    const len = elementsCollection ? elementsCollection.length : 0;
    this.currentTabIndex = (this.currentTabIndex <= 0) ? (len - 1) : this.currentTabIndex;
    return elementsCollection[ this.currentTabIndex ];
  }

  activateFirstResultsRow(): void {
    const firstRow = this.getFirstRow();
    firstRow.focus();
    this.dispatchSearchFieldShouldUpdateSignal(firstRow.getAttribute('data-title') as string);
    this.currentTabIndex = 0;
  }
}
