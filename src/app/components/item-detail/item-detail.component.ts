import {Component, OnInit} from '@angular/core';
import {BeerInterface} from '../../interfaces/beer.interface';
import {ValuesCentralService} from '../../services/values-central.service';

@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.scss']
})
export class ItemDetailComponent implements OnInit {
  public beer?: BeerInterface;

  constructor(private vcService: ValuesCentralService) { }

  ngOnInit(): void {
    this.vcService.renderDetailSignal.subscribe((beer) => {
      this.beer = beer;
    });
  }

  get displayableDataMap(): any {
    if (this.beer) {
      return {
        Tagline: this.beer.tagline,
        Volume: `${this.beer.volume.value} ${this.beer.volume.unit}`,
        'First brewed on': this.beer.first_brewed,
        'Food Pairing': this.beer.food_pairing.map( foodPairing => {
          return `<span class="food-pairing">${foodPairing}</span>`;
        }).join(''),
        'Basic Ingredients': Object.keys(this.beer.ingredients).map( ingredientName => {
          return `<span class="food-pairing">${this.ucFirst(ingredientName)}</span>`;
        }).join(''),
        Description : this.beer.description,
      };
    }
    return {};
  }

  ucFirst(strVar: string): string{
    return strVar[0].toUpperCase() + strVar.substr(1);
  }

}
