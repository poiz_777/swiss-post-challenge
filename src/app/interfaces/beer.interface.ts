export interface GenericUnitValueInterface {
  value: number;
  unit: string;
}

export interface MashTempInterface {
  temp: GenericUnitValueInterface;
  duration: number;
}

export interface IngredientInterface {
  name: string;
  amount: GenericUnitValueInterface;
}

export interface ExtraIngredientInterface extends IngredientInterface{
  add?: string;
  attribute?: string;
}

export interface BeerInterface {
  id: number;
  name: string;
  tagline: string;
  first_brewed: string;
  description: string;
  image_url: string;
  abv: number;
  ibu: number;
  target_fg: number;
  target_og: number;
  ebc?: any;
  srm?: any;
  ph: number;
  attenuation_level: number;
  volume: GenericUnitValueInterface;
  boil_volume: GenericUnitValueInterface;
  method: {
    mash_temp: Array<MashTempInterface>;
    fermentation: {
      temp: GenericUnitValueInterface;
    };
    twist: string;
  };
  ingredients: {
    malt: Array<IngredientInterface>;
    hops: Array<ExtraIngredientInterface>;
    yeast: string;
  };
  food_pairing: Array<string>;
  brewers_tips: string;
  contributed_by: string;
}
