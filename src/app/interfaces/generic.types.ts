import {HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BeerInterface} from './beer.interface';

export type HttpErrorHandlerType =
  <T> (operation?: string, result?: T) => (error: HttpErrorResponse) => Observable<T>;
