import {HttpHeaders} from '@angular/common/http';

interface GenericConfigInterface {
  [ configName: string ]: string;
}

interface HeaderOptionsInterface {
  headers: HttpHeaders;
}


export class HeaderConfig {
  private httpOpts: HeaderOptionsInterface;

  constructor(httpConfig: GenericConfigInterface = {'Content-Type':  'application/json'} ) {
    this.httpOpts = { headers: new HttpHeaders( httpConfig ) };
  }

  get httpOptions(): HeaderOptionsInterface {
    return this.httpOpts;
  }

  set httpOptions(value: HeaderOptionsInterface) {
    this.httpOpts = value;
  }
}

