import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import {catchError, debounceTime, shareReplay} from 'rxjs/operators';
import { ErrorManager } from './error.manager.service';
import { BeerInterface } from '../interfaces/beer.interface';
import { fromArray } from 'rxjs/internal/observable/fromArray';
import {HttpErrorHandlerType} from '../interfaces/generic.types';


@Injectable({
  providedIn: 'root'
})
export class SearchBrokerService {
  beerStockEndpoint = 'https://api.punkapi.com/v2/beers';
  private manageError: HttpErrorHandlerType;

  constructor( private http: HttpClient, errorManager: ErrorManager ) {
    this.manageError = errorManager.createManageError('SearchBrokerService');
  }

  /**
   * GIVEN A KEYWORD REPRESENTING AN ITEM'S FULL OR PARTIAL NAME,
   * FETCHES A COLLECTION OF ITEMS [IPA BEERS] FROM THE SPECIFIED ENDPOINT... [ `this.beerStockEndpoint` ]
   */
  fetchSuggestionsByKeyWord(keyword: string): Observable<BeerInterface[]> {
    keyword = keyword.trim();
    // BAIL EARLY IF WE DON'T HAVE A KEYWORD TO AVOID FETCHING ENTIRE PAYLOAD.
    // HOWEVER, THIS METHOD SHOULD ONLY BE CALLED WITH AN EXPLICIT KEYWORD PARAMETER.
    if (!keyword) {
      return fromArray([]);
    }
    // URL-ENCODE SEARCH-TERM AND ADD IT TO THE HTTP PARAMS AS OPTIONS.
    keyword = keyword.replace(/([\[\].\-+{}()?\/:])/gim, '\\$1');
    const options = keyword ?
      { params: (new HttpParams()).set('beer_name', keyword) } : {};
    return this.http.get<BeerInterface[]>(this.beerStockEndpoint, options)
      .pipe(
        shareReplay(),
        catchError(this.manageError('fetchSuggestionsByKeyWord', [])),
        debounceTime(15000)
      );
  }
}
