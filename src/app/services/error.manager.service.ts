import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { ErrorMessageService } from './error.message.service';
import { HttpErrorHandlerType } from '../interfaces/generic.types';


@Injectable()
export class ErrorManager {
  constructor(private errorMessageService: ErrorMessageService) { }

  /**
   * CREATES A CURRIED `ManageError$` fUNCTION THAT IS AWARE OF THE SERVICE NAME
   */
  createManageError = (serviceName = ''): HttpErrorHandlerType => {
    return <T>(operation = 'operation', result = {} as T) =>
      this.manageError(serviceName, operation, result);
  }

  /**
   * RETURNS A FUNCTION THAT MANAGES API-RELATED OPERATIONS FAILURES.
   * THIS ERROR MANAGER LETS THE APP CONTINUE TO RUN AS IF NO ERROR HAD OCCURRED...
   * @param serviceName = NAME OF DATA SERVICE THAT ATTEMPTED THE OPERATION
   * @param operation - NAME OF THE OPERATION THAT FAILED
   * @param result - OPTIONAL VALJUE TO RETURN AS THE OBSERVABLE RESULT
   */
  manageError<T>(serviceName = '', operation = 'operation', result = {} as T): (error: HttpErrorResponse) => Observable<T> {
    return (error: HttpErrorResponse): Observable<T> => {
      console.error(error);

      const message = (error.error instanceof ErrorEvent) ?
        error.error.message :
       `server returned code ${error.status} with body "${error.error}"`;

      // TODO: better job of transforming error for user consumption
      this.errorMessageService.add(`${serviceName}: ${operation} failed: ${message}`);

      // LET THE APP KEEP RUNNING BY RETURNING A SAFE RESULT
      return of( result );
    };

  }
}
