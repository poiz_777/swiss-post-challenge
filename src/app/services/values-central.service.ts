import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';
import {BeerInterface} from '../interfaces/beer.interface';

@Injectable({
  providedIn: 'root'
})
export class ValuesCentralService {
  renderDetailSignal: Subject<BeerInterface>;
  abortOperationsSignal: Subject<boolean>;
  itemWasSelectedSignal: Subject<BeerInterface>;
  updateFieldValueSignal: Subject<string>;
  refocusSearchFieldSignal: Subject<null>;
  searchButtonTriggeredStream: Subject<Event>;

  constructor() {
    this.updateFieldValueSignal = new Subject<string>();
    this.renderDetailSignal = new Subject<BeerInterface>();
    this.itemWasSelectedSignal = new Subject<BeerInterface>();
    this.abortOperationsSignal = new Subject<boolean>();
    this.refocusSearchFieldSignal = new Subject<null>();
    this.searchButtonTriggeredStream = new Subject<Event>();
  }

  broadcastItemWasSelectedSignal(beer?: BeerInterface): void {
    this.itemWasSelectedSignal.next(beer);
  }

  broadcastUpdateFieldValueSignal(fieldValue: string): void {
    this.updateFieldValueSignal.next(fieldValue);
  }

  broadcastRenderDetailSignal(beer?: BeerInterface): void {
    this.renderDetailSignal.next(beer);
  }

  broadcastAbortOperationsSignal(status: boolean): void {
    this.abortOperationsSignal.next(status);
  }

  broadcastRefocusSearchFieldSignal(): void {
    this.refocusSearchFieldSignal.next(null);
  }

  updateSearchButtonTriggeredStream(event: Event): void {
    this.searchButtonTriggeredStream.next(event);
  }
}
