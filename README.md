# SwissPostChallenge

This ReST-Based Search App is built on top of the `Version 11.2.11` of `Angular Framework` and was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.10.

## About the App.

This App simply interfaces with a Remote Service via ReST API to filter and fetch Items ( IPA Beers ) based on user-supplied keywords. 
The returned Payload is then rendered as a «Choice Field» or «DropDown» allowing the User to choose from a list of possible options.
Key-Strokes like`DOWN ARROW`, `UP ARROW`, `TAB KEY` / `ENTER KEY` and `ESCAPE KEY` has specific Functions herein as well viz:
`Navigate Down the List`, `Navigate Up the List`, `Choose current Item under Focus` and `Abort & Reset Search` respectively.

## Setup

After cloning the Repository, enter the repo-directory by running: `cd $path-to-repo`.
The quickest way to see results without needing any elaborate setup is to run the App in dev mode by issuing the following commands on the Terminal.

First install dependencies by running: `yarn install` or `npm install`.  Once the dependencies are resolved, you may want to run: `yarn start` or `npm run start` to start the Dev. Server.

Afterwards, using your preferred Web-Browser, you may navigate to : [http://localhost:4200/](http://localhost:4200/)

Click on the `Search Icon` in the Top-Right corner to `toggle` the 
View-Pane and thus expose or blind the Search Input Field... 

Start interacting with the App to see if it meets the Standards of the Challenge... (including Code-Reviews - of course)...
 
## Troubleshooting

If you experience any problems during the installation or setup process relating to `Jasmine`, try bumping up the version of `Jasmine` to the `lastest`.

## Tests
No Test was written for the App due to `Time constraints`.

## Screenshot
![Desktop Screenshot: #1](https://bitbucket.org/poiz_777/swiss-post-challenge/raw/6626a39504a1b03271439c9b4df78bd0eb15c1dc/src/assets/images/screenshot-desktop-1.png)
![Desktop Screenshot: #2](https://bitbucket.org/poiz_777/swiss-post-challenge/raw/6626a39504a1b03271439c9b4df78bd0eb15c1dc/src/assets/images/screenshot-desktop-2.png)
![Desktop Screenshot: #3](https://bitbucket.org/poiz_777/swiss-post-challenge/raw/6626a39504a1b03271439c9b4df78bd0eb15c1dc/src/assets/images/screenshot-desktop-3.png)
![Mobile Screenshot: #1](https://bitbucket.org/poiz_777/swiss-post-challenge/raw/6626a39504a1b03271439c9b4df78bd0eb15c1dc/src/assets/images/screenshot-mobile-1.png)
![Mobile Screenshot: #2](https://bitbucket.org/poiz_777/swiss-post-challenge/raw/6626a39504a1b03271439c9b4df78bd0eb15c1dc/src/assets/images/screenshot-mobile-2.png)
![Mobile Screenshot: #3](https://bitbucket.org/poiz_777/swiss-post-challenge/raw/6626a39504a1b03271439c9b4df78bd0eb15c1dc/src/assets/images/screenshot-mobile-3.png)

